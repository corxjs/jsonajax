class Request{
    constructor(){
        this.xhr = new XMLHttpRequest();
    }
    get(url,callback){
       // console.log(this); //Burda class ı gösteriyor
        this.xhr.open("GET",url);
        
        this.xhr.onload =()=>{
            if(this.xhr.status == 200){
            callback (null,JSON.parse(this.xhr.responseText));
        }else{
            callback("ERROR",null)
        }
        };
  
        this.xhr.send();       
    }
    post(url,data,callback){
        this.xhr.open("POST",url);
        this.xhr.setRequestHeader("Content-type","application/json");
        this.xhr.onload = ()=>{
            
        if(this.xhr.status === 201){
            callback(null,this.xhr.responseText);
        }else{
            callback("ERROR",null);
        }
    }
        this.xhr.send(JSON.stringify(data));
    }
    put(url,data,callback){
        this.xhr.open("PUT",url);
        this.xhr.setRequestHeader("Content-type","application/json");
        this.xhr.onload=()=>{
            if(this.xhr.status == 200){
                callback(null,this.xhr.responseText)
            }else{
                callback("ERROR",null)
            }
        }
        this.xhr.send(JSON.stringify(data))
    }
    delete(url,callback){
        // console.log(this); //Burda class ı gösteriyor
         this.xhr.open("DELETE",url);
         
         this.xhr.onload =()=>{
             if(this.xhr.status == 200){
             callback (null,"SUCCESSFUL");
         }else{
             callback("ERROR !",null)
         }
         };
   
         this.xhr.send();       
     }
}

