# AJAXJSON
This script allows you to easily perform GET,POST,PUT, delete operations.Only JSON can be sent as a parameter for PUT and POST operations.

example :
```javascript
    const istek = new Request();
```
- GET REQUEST
```javascript
    istek.get("https://jsonplaceholder.typicode.com/users/1",(err,res)=>{
    if(err === null){
        console.log(res)
    }else{
        console.log(err);
    }
})
```
- POST REQUEST
```javascript
istek.post("https://jsonplaceholder.typicode.com/users",{userId: 1,title: "TEST CORX"},(err,res)=>{
    if(err == null){
        console.log(res);
    }else{
        console.log(err);
    }
})
```
- PUT REQUEST
```javascript
istek.put("https://jsonplaceholder.typicode.com/albums/1",{userId: 1,title: "cer"},(err,res)=>{
    if(err == null){
        console.log(res);
    }else{
        console.log(err);
    }
})
```
- DELETE REQUEST
```javascript
istek.delete("https://jsonplaceholder.typicode.com/users/1",(err,res)=>{
    if(err === null){
        console.log(res)
    }else{
        console.log(err);
    }
})
```